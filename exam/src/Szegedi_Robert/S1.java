package Szegedi_Robert;

public class S1{
}

class A{
}

class B extends A{
    D d;
    private long t;
    public void b(){}
}

class C{
    public void bb(B b){}
}

class D{
    E e;
    F f;
    G g;
    D(){
        this.e=new E();
    }
    void ff(F f){}
    void gg(G g){}

    public void met1(int i){}
}
class E{
    public void met1(){}
}

class F{
    public void n(String s){}
}

class G{
    public double met3(){return 0;}

}
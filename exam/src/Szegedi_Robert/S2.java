package Szegedi_Robert;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class S2 extends JFrame {
    JTextField textField1;
    JTextField textField2;
    JTextField textField3;
    JButton button;

    S2(){
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(500, 500);
        setVisible(true);
    }

    public void init(){
        this.setLayout(null);

        textField1 = new JTextField();
        textField1.setBounds(200,50,100,50);

        textField2 = new JTextField();
        textField2.setBounds(200,150,100,50);

        textField3 = new JTextField();
        textField3.setBounds(200,250,100,50);
        textField3.setEditable(false);

        button = new JButton();
        button.setBounds(200,350,100,50);
        button.setText("Sum");

        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    int a = Integer.parseInt(textField1.getText());
                    int b = Integer.parseInt(textField2.getText());
                    int c = 0;

                    c = a + b;
                    textField3.setText(String.valueOf(c));
                }catch (NumberFormatException numberFormatException){
                    textField3.setText("Error!");
                }
            }
        });

        add(textField1);
        add(textField2);
        add(textField3);
        add(button);
    }


    public static void main(String[] args) {
        new S2();
    }
}




